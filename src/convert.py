import pandas as pd
from collections import defaultdict
from tqdm import tqdm
from os import mkdir
from os.path import abspath, isdir
import click


def _read_biom_file(fp, delimiter):
    df = pd.read_csv(fp, index_col=0, delimiter=delimiter)
    return [x for x in df.iterrows()]

def _convert_to_dict(l):
    nested_dict = lambda: defaultdict(nested_dict)
    sample_dict = nested_dict()

    for i in tqdm(l):
        row, items = i
        if items["taxonomy"] != "Unassigned":
            tax_tree = items["taxonomy"].split(";")
            for tax in tax_tree:
                if tax.split("__")[1] != "":
                    c = tax[0:3]
                    for sample in items.index.values[:-1]:
                        sample_count = items[sample]
                        try:
                            sample_dict[sample][c][tax] += sample_count
                        except:
                            sample_dict[sample][c][tax] = sample_count
    return sample_dict


def _out_to_csv(d, dir, delimiter="\t"):
    if isdir(dir) != True:
        mkdir(dir)

    for t in d[d.keys()[0]].keys():
        fp = "%s/%s.csv" % (abspath(dir),t)
        tax_tree = {}
        for sample, items in d.iteritems():
            tax_tree[sample] = items[t]
        pd.DataFrame.from_dict(tax_tree).T.to_csv(fp, sep=str(delimiter))


@click.command()
@click.option("--fp", required=True, help="Filepath to tab seperated biom file, this could be a URL.")
@click.option("--i_delimiter", default="\t", help="Delimiter to use for reading in the file. If None, it will automatically default to tab seperated.")
@click.option("--o_dir", required=True, help="Directory for exporting the files, if it doesn't exist then it will be created.")
@click.option("--o_delimiter", default="\t", help="Demimiter to use for exporting the data. If None, it will automatically default to tab seperated.")
def _run(fp, i_delimiter, o_dir, o_delimiter):
    lines = _read_biom_file(fp, i_delimiter)
    d = _convert_to_dict(lines)
    _out_to_csv(d, o_dir, o_delimiter)

if __name__ == "__main__":
    _run()
